# Config Server (Floricultura)
Servidor de configurações de aplicações em estrutura de Micro-serviços.

#### Spring Cloud Config Server

<img src="spring-config-server.png" alt="Servidor de COnfigurações">

 - Os microsserviços são preparados para um ambiente (cloud), cuja precificação é diretamente relacionada à quantidade de máquinas e ao uso de seus recursos de infraestrutura. Para reduzir esse custo, aplicações de microsserviços se encaixam bem, pois é possível escalar automaticamente, de acordo com a demanda, e em questão de segundos, pedaços do que antes era uma única aplicação. Nesse cenário, configurar manualmente os servidores com as configurações necessárias para cada aplicação é impraticável.
 - Um servidor de configuração é o lugar central para definir as configurações dos serviços
 - Todas as configurações dos microsserviços devem ficar externalizadas e centralizadas
 - O `Spring Config Server` é uma implementação do servidor do projeto `Spring Cloud`
 - Sobre a integração dos microsserviços com o servidor de configuração
 - Para tal, devemos configurar o *nome* do microsserviço, *profile* e *URL* do Config Server
 - Existem várias formas de definir um repositório de configurações, entre elas o GitHub.

 ##### Configurações
 - Anotação `@EnableConfigServer` deve ser inserida na classe principal
 - Configurações em `application.yml/properties`:
 
 ```
 server:
  port: 8888
  
spring:
  profiles:
    active: native
  cloud:
    config:
      server:
        native:
          search-locations: C:/Users/Kamila/Documents/Projetos/Java/microservicos/config/microservice-repo
 ```
 - Exemplo de requisições: 
    - GET: `http://localhost:8888/fornecedor/default`
    - GET: `http://localhost:8888/fornecedor/dev` // retorna arquivo com final "-dev"
    - GET: `http://localhost:8888/fornecedor/prod` // retorna arquivo com final "-prod"

##### Migrando configurações para o Git

 - Retiramos a configuração em application.yml `spring.profiles.active: native` que indica a busca pelo arquivo nos arquivos do servidor e adicionamos o endereço do repositório.
```
spring:
  cloud:
    config:
      server:
        git:
          uri: https://gitlab.com/java-kamila/microservices-alura/microservice-repo.git # add .git       
          search-paths: fornecedor  #folder no repositorio: fornecedor/fornecedor.yml
```
 - Para repositórios privados, que é o indicado, inserir usuário e senha de acesso ao repositório:
 ```
 spring.cloud.config.server.git.user.name=username
 spring.cloud.config.server.git.user.password=password
```